package fr.cea.kemanager.key.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets KeyInfo
 */
public enum KeyInfo {
  
  DPOS_SK("DPOS_SK"),
  
  DPOS_PK("DPOS_PK"),
  
  TRANS_SK("TRANS_SK"),
  
  TRANS_PK("TRANS_PK"),
  
  FHE_SK("FHE_SK"),
  
  FHE_PK("FHE_PK"),
  
  FHE_EVK("FHE_EVK");

  private String value;

  KeyInfo(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static KeyInfo fromValue(String text) {
    for (KeyInfo b : KeyInfo.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

