package fr.cea.kemanager.key.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.cea.kemanager.lib.json.BytesDeserializer;
import fr.cea.kemanager.lib.json.BytesSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * KeyObject
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-31T13:18:13.107Z")

public class KeyObject  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("key")
  private byte[] key = null;

  /**
   * Gets or Sets keyInfo
   */
  public enum KeyInfoEnum {
    DPOS_SK("DPOS_SK"),
    
    DPOS_PK("DPOS_PK"),
    
    TRANS_SK("TRANS_SK"),
    
    TRANS_PK("TRANS_PK"),
    
    FHE_SK("FHE_SK"),
    
    FHE_PK("FHE_PK"),
    
    FHE_EVK("FHE_EVK");

    private String value;

    KeyInfoEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static KeyInfoEnum fromValue(String text) {
      for (KeyInfoEnum b : KeyInfoEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("keyInfo")
  private KeyInfoEnum keyInfo = null;

  @JsonProperty("requestId")
  private String requestId = null;

  @JsonProperty("checksum")
  private String checksum = null;

  public KeyObject key(byte[] key) {
    this.key = key;
    return this;
  }

   /**
   * Get key
   * @return key
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  //@JsonSerialize(using = BytesSerializer.class)
  public byte[] getKey() {
    return key;
  }

  //@JsonDeserialize(using = BytesDeserializer.class)  
  public void setKey(byte[] key) {
    this.key = key;
  }

  public KeyObject keyInfo(KeyInfoEnum keyInfo) {
    this.keyInfo = keyInfo;
    return this;
  }

   /**
   * Get keyInfo
   * @return keyInfo
  **/
  @ApiModelProperty(example = "public key", required = true, value = "")
  @NotNull


  public KeyInfoEnum getKeyInfo() {
    return keyInfo;
  }

  public void setKeyInfo(KeyInfoEnum keyInfo) {
    this.keyInfo = keyInfo;
  }

  public KeyObject requestId(String requestId) {
    this.requestId = requestId;
    return this;
  }

   /**
   * Get requestId
   * @return requestId
  **/
  @ApiModelProperty(example = "request_id-abcd-xytzssqd", required = true, value = "")
  @NotNull


  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public KeyObject checksum(String checksum) {
    this.checksum = checksum;
    return this;
  }

   /**
   * Get checksum
   * @return checksum
  **/
  @ApiModelProperty(example = "checksum HASH for all the keys", required = true, value = "")
  @NotNull


  public String getChecksum() {
    return checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeyObject keyObject = (KeyObject) o;
    return Objects.equals(this.key, keyObject.key) &&
        Objects.equals(this.keyInfo, keyObject.keyInfo) &&
        Objects.equals(this.requestId, keyObject.requestId) &&
        Objects.equals(this.checksum, keyObject.checksum);
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, keyInfo, requestId, checksum);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class KeyObject {\n");
    
    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    keyInfo: ").append(toIndentedString(keyInfo)).append("\n");
    sb.append("    requestId: ").append(toIndentedString(requestId)).append("\n");
    sb.append("    checksum: ").append(toIndentedString(checksum)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

