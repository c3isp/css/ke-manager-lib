package fr.cea.kemanager.encrypt.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.cea.kemanager.lib.json.*;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * EncryptObjectRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-31T13:18:13.107Z")

public class EncryptObjectRequest  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("binaryObj")  
  private String binaryObj = null;

  @JsonProperty("objName")
  private String objName = null;

  @JsonProperty("urlToBack")
  private String urlToBack = null;

  public EncryptObjectRequest binaryObj(String binaryObj) {
    this.binaryObj = binaryObj;
    return this;
  }

   /**
   * Get binaryObj
   * @return binaryObj
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  //@JsonSerialize(using = BytesSerializer.class)
  public String getBinaryObj() {
    return this.binaryObj;
  }

  //@JsonDeserialize(using = BytesDeserializer.class)  
  public void setBinaryObj(String binaryObj) {
    this.binaryObj = binaryObj;
  }

  public EncryptObjectRequest objName(String objName) {
    this.objName = objName;
    return this;
  }

   /**
   * Get objName
   * @return objName
  **/
  @ApiModelProperty(example = "obbj-name.pk", required = true, value = "")
  @NotNull


  public String getObjName() {
    return objName;
  }

  public void setObjName(String objName) {
    this.objName = objName;
  }

  public EncryptObjectRequest urlToBack(String urlToBack) {
    this.urlToBack = urlToBack;
    return this;
  }

   /**
   * an identifier CTI used for selecting this specific bundle
   * @return urlToBack
  **/
  @ApiModelProperty(required = true, value = "an identifier CTI used for selecting this specific bundle")
  @NotNull


  public String getUrlToBack() {
    return urlToBack;
  }

  public void setUrlToBack(String urlToBack) {
    this.urlToBack = urlToBack;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EncryptObjectRequest encryptObjectRequest = (EncryptObjectRequest) o;
    return Objects.equals(this.binaryObj, encryptObjectRequest.binaryObj) &&
        Objects.equals(this.objName, encryptObjectRequest.objName) &&
        Objects.equals(this.urlToBack, encryptObjectRequest.urlToBack);
  }

  @Override
  public int hashCode() {
    return Objects.hash(binaryObj, objName, urlToBack);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EncryptObjectRequest {\n");
    
    sb.append("    binaryObj: ").append(toIndentedString(binaryObj)).append("\n");
    sb.append("    objName: ").append(toIndentedString(objName)).append("\n");
    sb.append("    urlToBack: ").append(toIndentedString(urlToBack)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


