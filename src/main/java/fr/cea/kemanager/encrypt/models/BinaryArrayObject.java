package fr.cea.kemanager.encrypt.models;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * BinaryArrayObject
 */
@Validated

public class BinaryArrayObject  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("binaryObj")
  private String binaryObj = null;

  @JsonProperty("objName")
  private String objName = null;

  @JsonProperty("requestId")
  private String requestId = null;

  @JsonProperty("otherProperties")
  @Valid
  private Map<String, String> otherProperties = new HashMap<>();

  public BinaryArrayObject binaryObj(String binaryObj) {
    this.binaryObj = binaryObj;
    return this;
  }

  /**
   * Get binaryObj
   * @return binaryObj
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getBinaryObj() {
    return binaryObj;
  }

  public void setBinaryObj(String binaryObj) {
    this.binaryObj = binaryObj;
  }

  public BinaryArrayObject objName(String objName) {
    this.objName = objName;
    return this;
  }

  /**
   * Get objName
   * @return objName
  **/
  @ApiModelProperty(example = "obbj-name.pk", required = true, value = "")
  @NotNull


  public String getObjName() {
    return objName;
  }

  public void setObjName(String objName) {
    this.objName = objName;
  }

  public BinaryArrayObject requestId(String requestId) {
    this.requestId = requestId;
    return this;
  }

  /**
   * Get requestId
   * @return requestId
  **/
  @ApiModelProperty(example = "request_id-abcd-xytzssqd", required = true, value = "")
  @NotNull


  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public BinaryArrayObject otherProperties(Map<String, String> otherProperties) {
    this.otherProperties = otherProperties;
    return this;
  }

  public BinaryArrayObject putOtherPropertiesItem(String key, String otherPropertiesItem) {
    this.otherProperties.put(key, otherPropertiesItem);
    return this;
  }

  /**
   * list of parameters with default values - 'offsetTranscrypting' the array of offset keystream, - 'field' to make transciphering or other param we wish
   * @return otherProperties
  **/
  @ApiModelProperty(required = true, value = "list of parameters with default values - 'offsetTranscrypting' the array of offset keystream, - 'field' to make transciphering or other param we wish")
  @NotNull


  public Map<String, String> getOtherProperties() {
    return otherProperties;
  }

  public void setOtherProperties(Map<String, String> otherProperties) {
    this.otherProperties = otherProperties;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BinaryArrayObject binaryArrayObject = (BinaryArrayObject) o;
    return Objects.equals(this.binaryObj, binaryArrayObject.binaryObj) &&
        Objects.equals(this.objName, binaryArrayObject.objName) &&
        Objects.equals(this.requestId, binaryArrayObject.requestId) &&
        Objects.equals(this.otherProperties, binaryArrayObject.otherProperties);
  }

  @Override
  public int hashCode() {
    return Objects.hash(binaryObj, objName, requestId, otherProperties);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BinaryArrayObject {\n");
    
    sb.append("    binaryObj: ").append(toIndentedString(binaryObj)).append("\n");
    sb.append("    objName: ").append(toIndentedString(objName)).append("\n");
    sb.append("    requestId: ").append(toIndentedString(requestId)).append("\n");
    sb.append("    otherProperties: ").append(toIndentedString(otherProperties)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

