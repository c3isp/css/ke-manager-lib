

package fr.cea.kemanager.client.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}
