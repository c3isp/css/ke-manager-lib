package fr.cea.kemanager.lib;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets ResponseModel
 */
public enum ResponseModel {
  
  SUCCESS("Success"),
  
  BAD_REQUEST("Bad_Request"),
  
  INTERNAL_ERROR("Internal_Error");

  private String value;

  ResponseModel(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ResponseModel fromValue(String text) {
    for (ResponseModel b : ResponseModel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

