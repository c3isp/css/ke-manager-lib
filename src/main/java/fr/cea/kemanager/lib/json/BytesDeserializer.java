package fr.cea.kemanager.lib.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class BytesDeserializer extends JsonDeserializer<byte[]> {
 
	@Override
	public byte[]  deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext)
			throws IOException, JsonProcessingException {
		// TODO Auto-generated method stub		        
		String bytearray = jsonparser.getText();
		return bytearray.getBytes();
	}	
}
