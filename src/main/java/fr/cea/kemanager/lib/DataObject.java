package fr.cea.kemanager.lib;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.Serializable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataObject
 */
@Validated

public class DataObject  implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @JsonProperty("dataContent")
  private String dataContent = null;

  @JsonProperty("dataName")
  private String dataName = null;

  @JsonProperty("checksum")
  private String checksum = null;

  @JsonProperty("urlToBack")
  private String urlToBack = null;

  @JsonProperty("otherProperties")
  @Valid
  private Map<String, String> otherProperties = new HashMap<>();

  public DataObject dataContent(String dataContent) {
    this.dataContent = dataContent;
    return this;
  }

  /**
   * Get dataContent
   * @return dataContent
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getDataContent() {
    return dataContent;
  }

  public void setDataContent(String dataContent) {
    this.dataContent = dataContent;
  }

  public DataObject dataName(String dataName) {
    this.dataName = dataName;
    return this;
  }

  /**
   * Get dataName
   * @return dataName
  **/
  @ApiModelProperty(example = "obbj-name.pk", required = true, value = "")
  @NotNull


  public String getDataName() {
    return dataName;
  }

  public void setDataName(String dataName) {
    this.dataName = dataName;
  }

  public DataObject checksum(String checksum) {
    this.checksum = checksum;
    return this;
  }

  /**
   * Get checksum
   * @return checksum
  **/
  @ApiModelProperty(example = "request_id-abcd-xytzssqd", required = true, value = "")
  @NotNull


  public String getChecksum() {
    return checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public DataObject urlToBack(String urlToBack) {
    this.urlToBack = urlToBack;
    return this;
  }

  /**
   * Get urlToBack
   * @return urlToBack
  **/
  @ApiModelProperty(example = "https://asynchronize", required = true, value = "")
  @NotNull


  public String getUrlToBack() {
    return urlToBack;
  }

  public void setUrlToBack(String urlToBack) {
    this.urlToBack = urlToBack;
  }

  public DataObject otherProperties(Map<String, String> otherProperties) {
    this.otherProperties = otherProperties;
    return this;
  }

  public DataObject putOtherPropertiesItem(String key, String otherPropertiesItem) {
    this.otherProperties.put(key, otherPropertiesItem);
    return this;
  }

  /**
   * list of parameters with default values - 'offsetTranscrypting' the array of offset keystream, - 'field' to make transciphering or other param we wish
   * @return otherProperties
  **/
  @ApiModelProperty(required = true, value = "list of parameters with default values - 'offsetTranscrypting' the array of offset keystream, - 'field' to make transciphering or other param we wish")
  @NotNull


  public Map<String, String> getOtherProperties() {
    return otherProperties;
  }

  public void setOtherProperties(Map<String, String> otherProperties) {
    this.otherProperties = otherProperties;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataObject dataObject = (DataObject) o;
    return Objects.equals(this.dataContent, dataObject.dataContent) &&
        Objects.equals(this.dataName, dataObject.dataName) &&
        Objects.equals(this.checksum, dataObject.checksum) &&
        Objects.equals(this.urlToBack, dataObject.urlToBack) &&
        Objects.equals(this.otherProperties, dataObject.otherProperties);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dataContent, dataName, checksum, urlToBack, otherProperties);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataObject {\n");
    
    sb.append("    dataContent: ").append(toIndentedString(dataContent)).append("\n");
    sb.append("    dataName: ").append(toIndentedString(dataName)).append("\n");
    sb.append("    checksum: ").append(toIndentedString(checksum)).append("\n");
    sb.append("    urlToBack: ").append(toIndentedString(urlToBack)).append("\n");
    sb.append("    otherProperties: ").append(toIndentedString(otherProperties)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

