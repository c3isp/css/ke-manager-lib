package fr.cea.kemanager.lib;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets TechnologyModel
 */
public enum TechnologyModel {
  
  STANDARD("Standard"),
  
  FHE("FHE");

  private String value;

  TechnologyModel(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static TechnologyModel fromValue(String text) {
    for (TechnologyModel b : TechnologyModel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

