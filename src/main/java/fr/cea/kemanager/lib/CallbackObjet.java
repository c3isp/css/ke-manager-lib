package fr.cea.kemanager.lib;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CallbackObjet
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-07T14:06:36.827Z")

public class CallbackObjet  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("urlToBack")
  private String urlToBack = null;

  /**
   * analysis message from Bundle Manager
   */
  public enum MessageEnum {
    SUCCESS("Success"),
    
    BAD_REQUEST("Bad_Request"),
    
    INTERNAL_ERROR("Internal_Error");

    private String value;

    MessageEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static MessageEnum fromValue(String text) {
      for (MessageEnum b : MessageEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("message")
  private MessageEnum message = null;

  @JsonProperty("infos")
  private String infos = null;

  @JsonProperty("requestDate")
  private String requestDate = null;

  public CallbackObjet urlToBack(String urlToBack) {
    this.urlToBack = urlToBack;
    return this;
  }

   /**
   * Get urlToBack
   * @return urlToBack
  **/
  @ApiModelProperty(example = "http://www.example.com/notification", required = true, value = "")
  @NotNull


  public String getUrlToBack() {
    return urlToBack;
  }

  public void setUrlToBack(String urlToBack) {
    this.urlToBack = urlToBack;
  }

  public CallbackObjet message(MessageEnum message) {
    this.message = message;
    return this;
  }

   /**
   * analysis message from Bundle Manager
   * @return message
  **/
  @ApiModelProperty(example = "Success", required = true, value = "analysis message from Bundle Manager")
  @NotNull


  public MessageEnum getMessage() {
    return message;
  }

  public void setMessage(MessageEnum message) {
    this.message = message;
  }

  public CallbackObjet infos(String infos) {
    this.infos = infos;
    return this;
  }

   /**
   * supplement information from 'message' field, in this field, when analysis request was successfully done, this will contain Identifier of analysis results which is stored in Cloud. Otherwise, this contains error information
   * @return infos
  **/
  @ApiModelProperty(example = "ResultID_Already_Stored_In_DPOS", required = true, value = "supplement information from 'message' field, in this field, when analysis request was successfully done, this will contain Identifier of analysis results which is stored in Cloud. Otherwise, this contains error information")
  @NotNull


  public String getInfos() {
    return infos;
  }

  public void setInfos(String infos) {
    this.infos = infos;
  }

  public CallbackObjet requestDate(String requestDate) {
    this.requestDate = requestDate;
    return this;
  }

   /**
   * Get requestDate
   * @return requestDate
  **/
  @ApiModelProperty(example = "2016-08-29T09.12.33.001", required = true, value = "")
  @NotNull

  @Valid

  public String getRequestDate() {
    return requestDate;
  }

  public void setRequestDate(String requestDate) {
    this.requestDate = requestDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CallbackObjet callbackObjet = (CallbackObjet) o;
    return Objects.equals(this.urlToBack, callbackObjet.urlToBack) &&
        Objects.equals(this.message, callbackObjet.message) &&
        Objects.equals(this.infos, callbackObjet.infos) &&
        Objects.equals(this.requestDate, callbackObjet.requestDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(urlToBack, message, infos, requestDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CallbackObjet {\n");
    
    sb.append("    urlToBack: ").append(toIndentedString(urlToBack)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    infos: ").append(toIndentedString(infos)).append("\n");
    sb.append("    requestDate: ").append(toIndentedString(requestDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

