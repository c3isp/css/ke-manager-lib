package fr.cea.kemanager.lib;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * ObjectParameterRequest
 */
@Validated

public class ObjectParameterRequest  implements Serializable {
	private static final long serialVersionUID = 1L;

	  @JsonProperty("Id")
	  private String id = null;

	  @JsonProperty("paramProperties")
	  @Valid
	  private Map<String, String> paramProperties = new HashMap<>();

	  public ObjectParameterRequest id(String id) {
	    this.id = id;
	    return this;
	  }

	  /**
	   * Get id
	   * @return id
	  **/
	  @ApiModelProperty(required = true, value = "")
	  @NotNull
	  
	  public String getId() {
	    return id;
	  }

	  public void setId(String id) {
	    this.id = id;
	  }

	  public ObjectParameterRequest paramProperties(Map<String, String> paramProperties) {
	    this.paramProperties = paramProperties;
	    return this;
	  }

	  public ObjectParameterRequest putParamPropertiesItem(String key, String paramPropertiesItem) {
	    this.paramProperties.put(key, paramPropertiesItem);
	    return this;
	  }

	  /**
	   * list of parameters with default values - 'contentString' the content file, - 'field' to make transciphering or other param we wish
	   * @return paramProperties
	  **/
	  @ApiModelProperty(required = true, value = 
			  "list of parameters with default values - 'contentString' the content file, - 'field' to make transciphering or other param we wish")
	  @NotNull
	  public Map<String, String> getParamProperties() {
	    return paramProperties;
	  }

	  public void setParamProperties(Map<String, String> paramProperties) {
	    this.paramProperties = paramProperties;
	  }


	  @Override
	  public boolean equals(java.lang.Object o) {
	    if (this == o) {
	      return true;
	    }
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    ObjectParameterRequest objectParameterRequest = (ObjectParameterRequest) o;
	    return Objects.equals(this.id, objectParameterRequest.id) &&
	        Objects.equals(this.paramProperties, objectParameterRequest.paramProperties);
	  }

	  @Override
	  public int hashCode() {
	    return Objects.hash(id, paramProperties);
	  }

	  @Override
	  public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class ObjectParameterRequest {\n");
	    
	    sb.append("    id: ").append(toIndentedString(id)).append("\n");
	    sb.append("    paramProperties: ").append(toIndentedString(paramProperties)).append("\n");
	    sb.append("}");
	    return sb.toString();
	  }

	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }
	
}

