package fr.cea.kemanager.lib;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets FHEModel
 */
public enum FHEModel {
  
  BLACK_LIST("BLACK_LIST"),
  
  PATTERN_MATCHING("PATTERN_MATCHING");

  private String value;

  FHEModel(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static FHEModel fromValue(String text) {
    for (FHEModel b : FHEModel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

